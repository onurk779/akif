package com.example.gezgi.akif.Manager;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by gezgi on 20.01.2018.
 */

public class SessionManager {
    SharedPreferences pref;

    SharedPreferences.Editor editor;

    Context mContext;

    int PRIVATE_MODE = 0;

    private static final String PREF_NAME = "SimTestPref";
    private static SessionManager mSessionManager;

    public static SessionManager getInstance() {
        if (mSessionManager == null) {
            mSessionManager = new SessionManager();
        }
        return mSessionManager;
    }

    public SessionManager() {

    }

    public void setContext(Context context) {
        this.mContext = context;
        pref = mContext.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public Context getContext() {
        return this.mContext;
    }

    public void set(String Key, String Value) {
        editor.putString(Key, Value);
        editor.commit();
    }

    public String get(String Key) {
        return pref.getString(Key, null);
    }

}
