package com.example.gezgi.akif;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telecom.Call;
import android.telecom.TelecomManager;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.gezgi.akif.MailService.MailSender;
import com.example.gezgi.akif.Manager.SessionManager;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {
    SendSmsTask mSendSmsTask;
    PendingIntent pi;
    SessionManager Session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        pi = PendingIntent.getActivity(getApplicationContext(), 0, intent, 0);

        Session = new SessionManager();
        Session = SessionManager.getInstance();
        Session.setContext(getApplicationContext());

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        Intent Settings = new Intent(this, SettingsActivity.class);
        startActivity(Settings);
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public class SendSmsTask extends AsyncTask<String, Void, Void> {
        @Override
        protected Void doInBackground(String... num) {

            SmsManager sms = SmsManager.getDefault();
            sms.sendTextMessage(num[0], null, "simtest", pi, null);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            Toast.makeText(getApplicationContext(), "Message Sent successfully!", Toast.LENGTH_LONG).show();
            Intent callIntent = new Intent(Intent.ACTION_CALL);
//            callIntent.setData(Uri.parse("tel:05426269979"));

            if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
//            startActivity(callIntent);

        }

        @Override
        protected void onCancelled(Void result) {
            super.onCancelled(result);
        }
    }

    @OnClick(R.id.button1)
    public void Start() {

        process();
    }

    public void process() {
        SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        mSendSmsTask = new SendSmsTask();
        mSendSmsTask.execute(SP.getString("num1", "NA"));
        Session.set("num1", "success");
        mSendSmsTask = new SendSmsTask();
        mSendSmsTask.execute(SP.getString("num2", "NA"));
        Session.set("num2", "success");
        mSendSmsTask = new SendSmsTask();
        mSendSmsTask.execute(SP.getString("num3", "NA"));
        Session.set("num3", "success");
    }


    public void SenderMail() {
        new Thread(new Runnable() {

            public void run() {

                try {
                    SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
                    String num1n = SP.getString("num1", "NA");
                    String num2n = SP.getString("num2", "NA");
                    String num3n = SP.getString("num3", "NA");
                    String num1 = Session.get("num1");
                    String num2 = Session.get("num2");
                    String num3 = Session.get("num3");
                    MailSender sender = new MailSender("akifakdogan@gmail.com", "32012378,a");
                    sender.sendMail("Test mail", num1n + ":" + num1 + "/n" + num2n + ":" + num2 + "/n" + num3n + ":" + num3 + "/n", "mehmetakif.akdogan@partner.turktelekom.com.tr", "mehmetakif.akdogan@partner.turktelekom.com.tr");

                } catch (Exception e) {

                    Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
                }

            }

        }).start();
    }
    @OnClick(R.id.mail)
    @RequiresApi(api = Build.VERSION_CODES.M)
    public void sim() {

        TelephonyManager tm = (TelephonyManager) getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE);
//        @SuppressLint("ServiceCast")
//        TelecomManager tma = (TelecomManager) getApplicationContext().getSystemService(Context.TELECOM_SERVICE);
//            tma.silenceRinger();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        String aa = tm.getSimSerialNumber();

        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
    }
}
